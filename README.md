# Praktikum Modul 4 Sistem Operasi

Perkenalkan kami dari kelas ``Sistem Operasi B Kelompok B08``, dengan anggota sebagai berikut:

| Nama                      | NRP        |
|---------------------------|------------|
|Dimas Prihady Setyawan     | 5025211184 |
|Yusuf Hasan Nazila         | 5025211225 |
|Ahda Filza Ghaffaru        | 5025211144 |

# Penjelasan Soal Nomor 1
Soal nomor 1 memerlukan analisis dataset kaggle berupa ``fifa-player-stats-database`` yang nantinya akan diambil satu set csv file yaitu ``FIFA23_official_data.csv`` lalu menganalisis serta mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain **Manchester City**. Semua hal tersebut dilakukan disebuah file ``storage.c`` yang dapat menngekstrak file zip serta melakukan analisis sesuai dengan kriteria yang ditentukan. Program tersebut juga perlu dijadikan sistem yang dibuat ke sebuah **Docker Container** agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal sehingga perlu dibuat ``Dockerfile`` yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan lalu membuat ``Docker Image`` menggunakan Docker CLI untuk mem*build* image. Setelah berhasil membuat *image*, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan ``Docker Container`` dan memeriksa output-nya. Selanjutnya, *image* yang telah dibuat, dipublish **Docker Hub** pada link berikut ``https://hub.docker.com/r/dimasps32/storage-app.``. Terakhir terapkan skala pada layanan menggunakan **Docker Compose** dengan instance sebanyak 5 lalu buat folder terpisah bernama ``Barcelona`` dan ``Napoli`` dan jalankan Docker Compose di sana.

## Penlejasan Soal 1a
Langkah pertama dalam proses ini adalah memperoleh data yang akan digunakan untuk analisis. Untuk melakukan ini, perlu membuat file bernama ``storage.c``. 

```
touch storage.c
```

Kemudian,  mengunduh dataset yang berisi informasi tentang pemain sepak bola dari Kaggle. Dataset ini mencakup data pemain sepak bola di seluruh dunia, termasuk Manchester Blue.

Untuk men-download dataset tersebut, dapat menggunakan perintah berikut di terminal atau command prompt:
```
kaggle datasets download -d bryanb/fifa-player-stats-database
```
Setelah menjalankan perintah tersebut, dataset akan diunduh dalam format .zip. Selanjutnya, mengekstrak file tersebut. Untuk memastikan bahwa semua langkah pengerjaan terkait dataset dilakukan di dalam file ``storage.c``, ekstrasi dapat dilakukan di ``storage.c`` dengan kode sebagai berikut.

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

```
Dalam kode tersebut, terdapat empat header file yang di-include, yaitu:

1. ``stdio.h``  : Header file ini berisi definisi untuk fungsi-fungsi standar input/output seperti ``printf()`` dan ``scanf()``. Ini memungkinkan penggunaan fungsi-fungsi ini dalam program.
2. ``stdlib.h`` : Header file ini berisi definisi untuk fungsi-fungsi standar seperti alokasi memori (``malloc()`` dan ``free()``), konversi string ke angka (``atoi()`` dan ``atof()``), pengurutan (``qsort()``) dan lain-lain. Dengan memasukkan header file ini,  fungsi-fungsi tersebut dapat digunakan dalam program.
3. ``string.h`` : Header file ini berisi definisi untuk fungsi-fungsi yang berkaitan dengan manipulasi string, seperti penggabungan string (``strcat()``), pembandingan string (``strcmp()``), menghitung panjang string (``strlen()``) dan banyak lagi. Dengan memasukkan header file ini, fungsi-fungsi tersebut dapat digunakan dalam program.
4. ``zlib.h``   : Header file ini berisi definisi untuk kompresi dan dekompresi data menggunakan algoritma kompresi zlib. Zlib adalah pustaka yang umum digunakan untuk kompresi data dalam format zlib, gzip, atau zip. Dengan memasukkan header file ini, fungsi-fungsi zlib dapat digunakan dalam program untuk melakukan kompresi atau dekompresi data.

Dengan memasukkan empat header file di atas, fungsi-fungsi yang telah didefinisikan dapat digunakan dalam header file tersebut dalam program. Selain header, terdapat dua define untuk ``MAX_PLAYERS`` berjumlah 1000 dan ``MAX_LINE_LENGTH`` berjumlah 2048.

```c
#define MAX_PLAYERS 1000
#define MAX_LINE_LENGTH 2048
```
Berikut adalah penerapan fungsi dari ``extractZip``

```c
int extractZip(const char* zipFilePath, const char* destFolderPath) {
    char command[256];
    sprintf(command, "unzip %s -d %s", zipFilePath, destFolderPath);

    if (system(command) == -1) {
        printf("Failed to extract zip file.\n");
        return -1;
    }
    return 0;
}

```
Fungsi tersebut, extractZip, bertujuan untuk mengekstrak file ZIP ke folder tujuan yang ditentukan. Berikut adalah penjelasan singkat tentang fungsi tersebut:

1. ``int extractZip(const char* zipFilePath, const char* destFolderPath)``: Fungsi ini menerima dua parameter, yaitu ``zipFilePath`` yang merupakan path/file path dari file ZIP yang akan diekstrak, dan destFolderPath yang merupakan path/directory path dari folder tujuan tempat hasil ekstraksi akan disimpan.
2. ``char command[256];``: Variabel command digunakan untuk menyimpan perintah yang akan dijalankan di terminal/command prompt untuk mengekstrak file ZIP.
3. ``sprintf(command, "unzip %s -d %s", zipFilePath, destFolderPath);``: Fungsi sprintf digunakan untuk memformat string command. Di sini, command akan berisi perintah "unzip" diikuti oleh ``zipFilePath`` (path file ZIP) dan destFolderPath (path folder tujuan) yang diberikan oleh pengguna.
4. ``if (system(command) == -1)``: Fungsi ``system()`` digunakan untuk menjalankan perintah yang ada dalam variabel command. Jika return value dari ``system(command)`` adalah -1, ini menunjukkan bahwa perintah tidak berhasil dijalankan.
5. ``printf("Failed to extract zip file.\n");``: Jika perintah ekstraksi gagal, pesan kesalahan akan dicetak ke layar.
6. ``return -1;``: Nilai -1 akan dikembalikan dari fungsi extractZip untuk menunjukkan kegagalan dalam ekstraksi file ZIP.
7. ``return 0;``: Nilai 0 akan dikembalikan dari fungsi extractZip untuk menunjukkan bahwa ekstraksi file ZIP berhasil dilakukan.

Dalam keseluruhan, fungsi extractZip ini mengambil file ZIP dan folder tujuan sebagai input, kemudian menjalankan perintah "unzip" untuk mengekstrak file ZIP ke folder tujuan. Jika ekstraksi berhasil, fungsi akan mengembalikan nilai 0, jika tidak, akan mengembalikan nilai -1.

Berikut adalah penerapannya dalam fungsi ``main``

```c
int main() {
    const char* zipFilePath = "fifa-player-stats-database.zip";
    const char* destFolderPath = "."; // Extract to current directory
    const char* csvFilePath = "FIFA23_official_data.csv";
    Player players[MAX_PLAYERS];

    if (extractZip(zipFilePath, destFolderPath) != 0) {
        printf("Extraction failed.\n");
        return -1;
    }
```
Fungsi main di atas adalah fungsi utama dalam program. Berikut adalah penjelasan singkat tentang fungsi tersebut:

1. ``int main()``: Fungsi main merupakan entry point dari program. Program akan dimulai dari sini.
2. ``const char* zipFilePath = "fifa-player-stats-database.zip";``: Variabel ``zipFilePath`` merupakan path/file path dari file ZIP yang akan diekstrak. Dalam contoh ini, file ZIP disebut "fifa-player-stats-database.zip".
3. ``const char* destFolderPath = ".";``: Variabel ``destFolderPath`` merupakan path/directory path dari folder tujuan tempat hasil ekstraksi akan disimpan. Dalam contoh ini, hasil ekstraksi akan disimpan di direktori saat ini.
4. ``const char* csvFilePath = "FIFA23_official_data.csv";``: Variabel ``csvFilePath`` merupakan path/file path dari file CSV yang akan digunakan untuk analisis lebih lanjut. Dalam contoh ini, file CSV disebut "FIFA23_official_data.csv".
5. ``Player players[MAX_PLAYERS];``: Variabel ``players`` merupakan sebuah array dari tipe data ``Player``. Array ini akan digunakan untuk menyimpan data pemain sepak bola setelah diekstraksi dari file ZIP.
6. ``if (extractZip(zipFilePath, destFolderPath) != 0) { ... }``: Fungsi extractZip dipanggil untuk mengekstrak file ZIP. Jika ekstraksi gagal (return value bukan 0), pesan kesalahan akan dicetak dan program akan berhenti dengan mengembalikan nilai -1.

Setelah potongan kode di atas, fungsi main akan melanjutkan dengan langkah-langkah berikutnya dalam program yang tidak disertakan dalam penjelasan ini. Namun, dari potongan kode tersebut, dapat diketahui bahwa fungsi main bertanggung jawab untuk mengatur langkah-langkah awal dalam program, seperti menentukan file ZIP yang akan diekstrak, folder tujuan ekstraksi, dan file CSV yang akan digunakan untuk analisis.

## Penjelasan soal 1b
Masih di ``storage.c`` program perlu membaca file CSV khusus bernama ``FIFA23_official_data.csv`` dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. Berikut adalah analisis dalam kode

```c
typedef struct {
    int id;
    char name[100];
    int age;
    char photo[200];
    char nationality[100];
    char flag[200];
    int overall;
    int potential;
    char club[100];
    char clubLogo[200];
    char value[20];
    char wage[20];
    int special;
    char preferredFoot[10];
    float internationalReputation;
    float weakFoot;
    float skillMoves;
    char workRate[50];
    char bodyType[50];
    char realFace[10];
    char position[50];
    char joined[50];
    char loanedFrom[100];
    char contractValidUntil[50];
    char height[10];
    char weight[10];
    char releaseClause[20];
    int kitNumber;
    int bestOverallRating;
} Player;

int readPlayersCSV(const char* csvFilePath, Player* players, int maxPlayers) {
    FILE* file = fopen(csvFilePath, "r");
    if (file == NULL) {
        printf("Failed to open CSV file: %s\n", csvFilePath);
        return -1;
    }
    char line[MAX_LINE_LENGTH];
    int count = 0;

    fgets(line, MAX_LINE_LENGTH, file); // Skip header line

    while (fgets(line, MAX_LINE_LENGTH, file) != NULL) {
        if (count >= maxPlayers) {
            printf("Maximum number of players reached.\n");
            break;
        }

        Player player;
        sscanf(line, "%d,%99[^,],%d,%199[^,],%99[^,],%199[^,],%d,%d,%99[^,],%199[^,],%19[^,],%19[^,],%d,%9[^,],%f,%f,%f,%199[^,],%49[^,],%9[^,],%49[^,],%199[^,],%99[^,],%49[^,],%9[^,],%9[^,],%19[^,],%d,%d",
       &player.id, player.name, &player.age, player.photo, player.nationality, player.flag,
       &player.overall, &player.potential, player.club, player.clubLogo, player.value, player.wage,
       &player.special, player.preferredFoot, &player.internationalReputation, &player.weakFoot,
       &player.skillMoves, player.workRate, player.bodyType, player.realFace, player.position,
       player.joined, player.loanedFrom, player.contractValidUntil, player.height, player.weight,
       player.releaseClause, &player.kitNumber, &player.bestOverallRating);


        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
            printf("Name: %s\n", player.name);
            printf("Age: %d\n", player.age);
            printf("Nationality: %s\n", player.nationality);
            printf("Club: %s\n", player.club);
            printf("Potential: %d\n", player.potential);
            printf("Photo URL: %s\n", player.photo);
            // Print other desired information

            printf("\n");
        }
        players[count++] = player;
    }
    fclose(file);
    return count;
}

```
Kode di atas mendefinisikan sebuah struct bernama ``Player`` yang memiliki beberapa anggota (fields) dengan tipe data yang berbeda-beda. Struct ini digunakan untuk merepresentasikan data seorang pemain sepak bola. Setiap anggota memiliki makna tertentu yang menggambarkan atribut-atribut pemain, seperti nama, usia, klub, dll.

Selanjutnya, terdapat fungsi ``readPlayersCSV`` yang bertugas membaca file CSV yang berisi data pemain dan menyimpannya dalam array dari tipe Player. Berikut adalah penjelasan singkat tentang fungsi tersebut:

1. ``int readPlayersCSV(const char* csvFilePath, Player* players, int maxPlayers)``: Fungsi ini menerima tiga parameter, yaitu path/file path dari file CSV yang akan dibaca (``csvFilePath``), array dari tipe ``Player`` yang akan menyimpan data pemain (``players``), dan batas maksimal jumlah pemain yang dapat dibaca (``maxPlayers``). Fungsi ini mengembalikan jumlah pemain yang berhasil dibaca.
2. ``FILE* file = fopen(csvFilePath, "r");``: Fungsi ``fopen`` digunakan untuk membuka file CSV dengan mode "r" (read). Jika file gagal dibuka, pesan kesalahan akan dicetak dan fungsi akan mengembalikan nilai -1.
3. ``char line[MAX_LINE_LENGTH];``: Variabel ``line`` merupakan array karakter yang digunakan untuk menyimpan setiap baris yang dibaca dari file CSV.
4. ``fgets(line, MAX_LINE_LENGTH, file);``: Fungsi ``fgets`` digunakan untuk membaca baris pertama (header) dari file CSV dan diabaikan.
5. ``while (fgets(line, MAX_LINE_LENGTH, file) != NULL) { ... }``: Fungsi ``fgets`` digunakan untuk membaca setiap baris data dari file CSV secara berulang hingga mencapai akhir file. Setiap baris akan diproses dalam loop ini.
6. ``sscanf(line, "...", &player.id, player.name, &player.age, ...);``: Fungsi sscanf digunakan untuk mem-parsing (menguraikan) data dari baris yang telah dibaca ke dalam variabel player. Format string yang diberikan kepada ``sscanf`` sesuai dengan format yang diharapkan dari data di dalam baris tersebut.
7. ``if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) { ... }``: Pada blok ini, data pemain yang telah diparsing akan diperiksa untuk kriteria tertentu. Jika pemain memenuhi kriteria (usia kurang dari 25, potensi di atas 85, dan bukan bermain di klub Manchester City), maka data pemain akan dicetak ke layar.
8. ``players[count++] = player;``: Data pemain yang telah diparsing dan memenuhi kriteria akan disimpan ke dalam array players.
9. Setelah selesai membaca file dan memproses semua baris data, file CSV ditutup dengan menggunakan ``fclose(file)``.
10. Fungsi mengembalikan jumlah pemain yang berhasil dibaca (``return count``).

Kode di atas digunakan untuk membaca file CSV yang berisi data pemain sepak bola dan menyimpannya ke dalam array players. Selain itu, juga dilakukan pengecekan terhadap kriteria tertentu pada data pemain dan mencetak informasi pemain yang memenuhi kriteria tersebut ke layar. Berikut adalah implementasi dalam fungsi ``main``

```c
    int playerCount = readPlayersCSV(csvFilePath, players, MAX_PLAYERS);
    if (playerCount < 0) {
        printf("Failed to read players CSV.\n");
        return -1;
    }
    printf("Analysis complete.\n");
    return 0;
}

```
Kode di atas merupakan bagian dari fungsi ``main`` yang merupakan entry point dari program. Berikut adalah penjelasan singkat tentang kode tersebut:

1. ``int playerCount = readPlayersCSV(csvFilePath, players, MAX_PLAYERS);``: Fungsi ``readPlayersCSV`` dipanggil dengan argumen ``csvFilePath`` (path/file path dari file CSV), ``players`` (array dari tipe ``Player``), dan ``MAX_PLAYERS`` (batas maksimal jumlah pemain yang dapat dibaca). Jumlah pemain yang berhasil dibaca akan disimpan dalam variabel ``playerCount``.
2. ``if (playerCount < 0) { ... }``: Dilakukan pengecekan apakah nilai ``playerCount`` kurang dari 0, yang menandakan bahwa ada kesalahan dalam membaca file CSV. Jika iya, maka pesan kesalahan akan dicetak dan program akan langsung keluar dari fungsi main dengan mengembalikan nilai -1.
3. ``printf("Analysis complete.\n");``: Setelah selesai membaca file CSV dan menganalisis data pemain, pesan "Analysis complete" akan dicetak ke layar.
4. ``return 0;``: Fungsi main mengembalikan nilai 0, menandakan bahwa program selesai dijalankan dengan sukses.

Kode ini merupakan bagian akhir dari fungsi main yang mengeksekusi proses pembacaan file CSV, pemrosesan data pemain, dan mencetak pesan akhir ke layar sebelum program selesai dieksekusi.

## Penjelasan soal 1c
Membuat Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan. Berikut adalah isi dari ``Dockerfile``.

```Dockerfile
# Base image
FROM gcc:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the source code into the container
COPY . /app

# Install necessary dependencies
RUN apt-get update && apt-get install -y zlib1g-dev

# Compile the source code
RUN gcc storage.c -lz -o storage

# Set the command to run when the container starts
CMD ["./storage"]
```
Dockerfile di atas digunakan untuk membangun Docker image yang akan menjalankan aplikasi ``storage.c`` di dalam sebuah kontainer Docker. Berikut adalah penjelasan singkat tentang setiap langkah dalam Dockerfile:

1. ``FROM gcc:latest``: Menggunakan base image dari ``gcc:latest``, yang berarti Docker image akan menggunakan image GCC terbaru sebagai dasar.
2. ``WORKDIR /app``: Mengatur direktori kerja di dalam kontainer menjadi ``/app``.
3. ``COPY . /app``: Menyalin seluruh isi dari direktori saat ini (yang berisi file-file yang dibutuhkan, termasuk storage.c) ke direktori ``/app`` di dalam kontainer.
4. ``RUN apt-get update && apt-get install -y zlib1g-dev``: Menjalankan perintah ``apt-get`` untuk meng-update dan meng-install paket ``zlib1g-dev`` yang diperlukan oleh aplikasi untuk kompresi dan dekompresi data menggunakan zlib.
5. ``RUN gcc storage.c -lz -o storage``: Mengkompilasi file ``storage.c`` menggunakan kompiler GCC dan menghasilkan executable ``storage``. Opsi ``-lz`` digunakan untuk menghubungkan library zlib yang diperlukan.
6. ``CMD ["./storage"]``: Menentukan perintah yang akan dijalankan saat kontainer dimulai. Di sini, perintah ``./storage`` akan dijalankan di dalam kontainer, menjalankan aplikasi ``storage`` yang telah dikompilasi.

Dengan menggunakan Dockerfile ini, kita dapat membangun Docker image yang mengemas aplikasi ``storage.c`` beserta dependensinya ke dalam kontainer Docker, sehingga memudahkan dalam mendistribusikan dan menjalankan aplikasi ini di berbagai lingkungan tanpa perlu melakukan setup yang rumit. Berikut adalah contoh pembuatan image dari Dockerfile pada Docker CLI

```
docker build -t my-storage-app .
docker run --rm my-storage-app
```
Pembuatan image Docker dan menjalankan kontainer menggunakan perintah-perintah berikut:

1. ``docker build -t my-storage-app .``: Perintah ini digunakan untuk membangun Docker image dengan nama my-storage-app. Opsi -t digunakan untuk memberikan tag atau nama pada image. Titik (.) menunjukkan bahwa Dockerfile yang akan digunakan berada di direktori saat ini. Pada langkah ini, Docker akan membaca Dockerfile di direktori saat ini dan memprosesnya untuk membangun image. Semua langkah yang didefinisikan dalam Dockerfile, seperti menyalin file, meng-install dependensi, dan mengkompilasi aplikasi, akan dieksekusi.
2. ``docker run --rm my-storage-app``: Setelah image berhasil dibangun, perintah ini digunakan untuk menjalankan kontainer berdasarkan image yang telah dibuat. Opsi --rm mengindikasikan bahwa kontainer akan dihapus secara otomatis setelah berhenti. Dalam konteks ini, kontainer akan menjalankan aplikasi storage yang telah dikompilasi di dalam image my-storage-app. Aplikasi akan berjalan sesuai dengan perintah yang telah ditentukan di dalam Dockerfile, yaitu CMD ["./storage"]. Setelah aplikasi selesai dieksekusi, kontainer akan berhenti dan akan dihapus karena opsi --rm yang telah ditentukan.

Dengan menggunakan perintah-perintah di atas, image Docker dapat dibangun dari Dockerfile dan menjalankan kontainer yang berjalan aplikasi storage.c. 

## Penjelasan soal 1d
Mem*publish* Docker Image sistem ke Docker Hub yang outputnya dapat diakses secara publik pada link format sebagai berikut ``https://hub.docker.com/r/dimasps32/storage-app``. Berikut adalah penerapannya dalam CLI.

```
docker tag my-storage-app:latest dimasps32/storage-app:latest
docker login
docker push dimasps32/storage-app:latest
```
Proses publish Docker image ke Docker Hub dengan langkah-langkah berikut:

1. ``docker tag my-storage-app:latest dimasps32/storage-app:latest``: Perintah ini digunakan untuk memberikan tag baru pada Docker image yang ingin dipublish. ``my-storage-app:latest`` adalah tag image lokal yang ingin dipublish, sedangkan ``dimasps32/storage-app:latest`` adalah tag baru yang akan diberikan saat mempublish image ke Docker Hub. Tag baru ini mengikuti format ``<username>/<repository>:<tag>``.
2. ``docker login``: Perintah ini digunakan untuk login ke akun Docker Hub. Ketika perintah dijalankan, user akan diminta untuk memasukkan username dan password akun Docker Hub.
3. ``docker push dimasps32/storage-app:latest``: Setelah login berhasil, perintah ini digunakan untuk mempublish image ke Docker Hub. Image dengan tag ``dimasps32/storage-app:latest`` akan diunggah ke Docker Hub dan tersedia secara publik. Proses ini membutuhkan waktu tergantung pada ukuran image dan kecepatan koneksi internet.

Setelah perintah ``docker push`` berhasil, Docker image ``dimasps32/storage-app:latest`` dapat diakses dan digunakan oleh orang lain melalui Docker Hub pada alamat yang sesuai.

## Penjelasan soal 1e
Dalam poin terakhir tersebut, terdapat kode Docker Compose yang digunakan untuk mengatur layanan dengan skala lima instance menggunakan Docker Compose. Berikut adalah penjelasan kode Docker Compose yang diberikan:
```yml
version: "3"
services:
  storage:
    image: dimasps32/storage-app
    ports:
      - "8082:8080"
    deploy:
      replicas: 5
```
1. ``version: "3"``: Mendefinisikan versi Docker Compose yang digunakan.
2. ``services``: Mendefinisikan layanan yang akan didefinisikan dalam Docker Compose.
3. ``storage``: Nama layanan yang diberikan, bisa diganti sesuai kebutuhan.
4. ``image: dimasps32/storage-app``: Menggunakan Docker image dimasps32/storage-app sebagai image yang akan digunakan dalam layanan ini.
5. ``ports: - "8082:8080"``: Mengaitkan port host 8082 dengan port container 8080. Ini memungkinkan akses ke layanan dari luar melalui port 8082.
6. ``deploy: replicas: 5``: Mengatur skala layanan dengan mendefinisikan jumlah replika yang akan dibuat. Dalam kasus ini, terdapat lima replika dari layanan storage.

Untuk menerapkan skala layanan menggunakan Docker Compose, perlu membuat folder terpisah dengan nama ``Barcelona`` atau ``Napoli`` (sesuai permintaan soal), lalu simpan file Docker Compose dengan nama ``docker-compose.yml`` di dalam folder tersebut. Setelah itu, buka terminal atau command prompt, arahkan ke direktori folder tersebut, dan jalankan perintah ``docker-compose up -d`` untuk menjalankan layanan dengan skala lima instance.

Dengan menggunakan Docker Compose, layanan ``storage`` akan diperbanyak menjadi lima replika yang siap menerima permintaan dan ditangani oleh Docker Swarm. Ini memungkinkan sistem untuk menangani lonjakan permintaan dengan lebih baik dan meningkatkan ketersediaan layanan.


### Penjelaasan Nomor 4
extract_module_path(const char* fullPath): Fungsi ini digunakan untuk mengambil path direktori modul dari suatu path file. Fungsi ini menggunakan regular expression untuk mencocokkan pola path modul. Jika pola ditemukan, fungsi akan mengembalikan path direktori modul tersebut. Jika tidak ditemukan atau terjadi kesalahan, fungsi akan mengembalikan NULL.

is_directory(const char *path): Fungsi ini digunakan untuk memeriksa apakah suatu path adalah direktori atau bukan. Fungsi ini menggunakan fungsi stat() untuk mendapatkan informasi tentang path tersebut. Jika path merupakan direktori, fungsi ini akan mengembalikan nilai 1. Jika bukan direktori atau terjadi kesalahan, fungsi ini akan mengembalikan nilai 0.

is_file_parted(const char *filepath): Fungsi ini digunakan untuk memeriksa apakah suatu file terbagi menjadi beberapa bagian atau tidak. Fungsi ini akan memeriksa keberadaan file dengan ekstensi ".001", ".002", dan seterusnya, yang menandakan bahwa file tersebut terbagi menjadi beberapa bagian. Jika semua bagian file ditemukan, fungsi ini akan mengembalikan nilai 1. Jika tidak ada bagian file yang ditemukan, fungsi ini akan mengembalikan nilai 0.

modularization(const char *path): Fungsi ini digunakan untuk membagi suatu file menjadi bagian-bagian yang lebih kecil. Fungsi ini membaca file dengan ukuran chunk_size (di sini ditetapkan 1024 byte) dan menulisnya ke file-file chunk terpisah dengan format "<nama_file>.001", "<nama_file>.002", dan seterusnya. Fungsi ini juga menghapus file asli setelah proses pembagian selesai.

combineChunks(const char* path): Fungsi ini digunakan untuk menggabungkan bagian-bagian file menjadi satu file utuh. Fungsi ini membaca file-file chunk dengan format "<nama_file>.001", "<nama_file>.002", dan seterusnya, dan menulis kontennya ke file yang memiliki nama asli. Setelah penggabungan selesai, fungsi ini akan menghapus file-file chunk.

read_combined_file_parts(const char *filepath, char *buf, size_t size, off_t offset): Fungsi ini digunakan untuk membaca konten gabungan dari beberapa bagian file. Fungsi ini membaca bagian-bagian file dengan format "<nama_file>.001", "<nama_file>.002", dan seterusnya, dan menggabungkannya menjadi satu buffer. Fungsi ini membaca sejumlah size byte dari offset offset dalam konten gabungan dan mengembalikan jumlah byte yang berhasil dibaca.

do_modular(const char *path): Fungsi ini digunakan untuk memproses secara rekursif direktori dan file-file di dalamnya. Jika suatu file memiliki ukuran lebih dari 1024 byte, fungsi ini akan memanggil fungsi modularization() untuk membagi file tersebut menjadi bagian-bagian yang lebih kecil.

do_demodular(const char* path): Fungsi ini digunakan untuk memproses secara rekursif direktori dan file-file di dalamnya. Jika suatu file terbagi menjadi beberapa bagian (diketahui dari ekstensi file), fungsi ini akan memanggil fungsi combineChunks() untuk menggabungkan file-file tersebut menjadi satu file utuh.

xmp_getattr(const char *path, struct stat *stbuf): Fungsi ini merupakan implementasi dari operasi getattr dalam FUSE. Fungsi ini mengembalikan informasi atribut (metadata) dari suatu path file atau direktori, seperti ukuran, waktu modifikasi, dan hak akses. Fungsi ini juga dapat memanggil fungsi do_modular() jika path adalah direktori dan memanggil fungsi is_file_parted() jika path adalah file.

xmp_access(const char *path, int mask): Fungsi ini merupakan implementasi dari operasi access dalam FUSE. Fungsi ini digunakan untuk memeriksa hak akses pengguna terhadap suatu path file atau direktori. Fungsi ini memanggil fungsi do_modular() jika path adalah direktori dan memanggil fungsi is_file_parted() jika path adalah file.

xmp_readlink(const char *path, char *buf, size_t size): Fungsi ini merupakan implementasi dari operasi readlink dalam FUSE. Fungsi ini digunakan untuk membaca isi sebuah symlink. Fungsi ini memanggil fungsi do_modular() jika path adalah direktori dan memanggil fungsi is_file_parted() jika path adalah file.

xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi): Fungsi ini merupakan implementasi dari operasi readdir dalam FUSE. Fungsi ini digunakan untuk membaca isi direktori. Fungsi ini memanggil fungsi do_modular() jika path adalah direktori dan memanggil fungsi is_file_parted() jika path adalah file.

xmp_open(const char *path, struct fuse_file_info *fi): Fungsi ini merupakan implementasi dari operasi open dalam FUSE. Fungsi ini digunakan untuk membuka suatu file. Fungsi ini memanggil fungsi do_modular() jika path adalah direktori dan memanggil fungsi is_file_parted() jika path adalah file.

xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi): Fungsi ini merupakan implementasi dari operasi read dalam FUSE. Fungsi ini digunakan untuk membaca konten suatu file. Fungsi ini memanggil fungsi read_combined_file_parts() jika file terbagi menjadi beberapa bagian dan membaca konten langsung jika file tidak terbagi.

xmp_unlink(const char *path): Fungsi ini merupakan implementasi dari operasi unlink dalam FUSE. Fungsi ini digunakan untuk menghapus suatu file. Fungsi ini memanggil fungsi do_demodular() jika file terbagi menjadi beberapa bagian.

write_log(const char *logmsg): Fungsi ini digunakan untuk menulis pesan log ke file "/home/index/fs_module.log". Fungsi ini membuka file log, menulis pesan log, dan menutup file log.

# Penjelasan Soal Nomor 5
Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program ``rahasia.c``. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.

## Penjelasan Soal 5A
Untuk poin A, kita hanya perlu melakukan proses download suatu zip file dari url google drive yang sudah diberikan melalui soal kemudian lakukan unzip file tersebut. Untuk implementasi di programnya hanya perlu melibatkan proses forking dengan tujuan untuk memanggil fungsi ``execv()``, ``wget`` untuk download dan ``unzip`` untuk proses unzip file ``rahasia.zip``. Berikut potongan kodenya.
```c
void downloadZip(){
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    if (child_id == 0){
        char *url = "https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=t&uuid=69f3b1f9-0761-4b5b-a766-346b671757fe&at=AKKF8vyHKxQz_HFZAZ1l_4H675Rb:1684825030868";
        char *argv[] = {"wget", "--no-check-certificate", "-q", "-O", "rahasia.zip", url, NULL};
        execv("/bin/wget", argv);
    } else {
        while ((wait(&status)) > 0);
        return;
    }  
}
void unzip(){
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        char *argv[] = {"unzip", "-qq", "rahasia.zip", "-d", "/home/ahda/sisop/modul4/rahasia", NULL};
        execv("/bin/unzip", argv);
    } else {
        while ((wait(&status)) > 0);
        return;
    }
}

pid_t pid = fork();

if (pid == 0){
  //poin A
    downloadZip();
    unzip();
}
```
## Penjelasan Soal 5B
Untuk poin soal B, dari folder rahasia yang telah di unzip, folder tersebut akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_b08 pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image. Berikut kurang lebih potongan kodenya.
```c
// Isi Docker-Compose.yml
version: "3"
services:
  myapp:
      image: rahasia_di_docker_b08
      container_name: container_rahasia
      command: tail -f /dev/null
      volumes:
      - /home/ahda/sisop/modul4/rahasia:/usr/share

// Poin B : Mount Folder ke Docker Container dengan Docker Compose
system("sudo docker pull ubuntu:focal");
system("sudo docker-compose up -d");
```

## Penjelasan Soal 5C
Untuk poin soal C, kita diperinta untuk membuat register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.'

Pada fungsi ``main()`` nantinya akan diperiksa user ingin melakukan opsi status yang mana (register, login, atau exit). Berikut potongan kodenya.
```c
// Poin C : Sistem Registrasi dan Login
      while(1){
    
        int status;
        printf("Welcome to Rahasia Encrypted Company, Choose Option:\n");
        printf("1. Register\n");
        printf("2. Login\n");
        printf("3. Exit\n");
        scanf("%d", &status);

        if (status == 1)
        {
          registerAccount();
        }
        else if (status == 2)
        {
          int logged = 0;
          int login = loginAccount();
          if (login == 1 && !logged)
          {
            logged = 1;
            printf("Success to Login\n");

            // tampilkan isi directory rahasia jika berhasil login
            system("sudo docker exec -it container_rahasia /bin/bash -c 'ls /usr/share/rahasia'");
          }
          else
          {
            printf("Failed to Login, Username and Passwords Does Not Match.\n");
          }
        }
        else break;
      }
```

Berikut adalah susunan struct ``User`` dan fungsi ``registerAccount()``, nantinya username dan password yang diinputkan oleh user akan disimpan pada suatu file .txt yang bernama ``users.txt``. Sebelum diinput ke dalam file, password akan di hash melalui fungsi ``GetMD5String()`` yang saya dapatkan melalui referensi ``https://www.programmingalgorithms.com/algorithm/md5/``.
```c
const char* GetMD5String(const char *msg, int mlen) {
	char str[33];
	strcpy(str, "");
	int j;
	unsigned *d = Algorithms_Hash_MD5(msg, strlen(msg));
	MD5union u;
	for (j = 0; j<4; j++) {
		u.w = d[j];
		char s[9];
		sprintf(s, "%02x%02x%02x%02x", u.b[0], u.b[1], u.b[2], u.b[3]);
		strcat(str, s);
	}

	return strdup(str);
}

typedef struct
{
  char username[MAX_CHAR];
  char password[MAX_CHAR];
} User;

void registerAccount()
{
  User user;
  printf("Enter your Username: ");
  scanf("%s", user.username);
  printf("Enter your Password: ");
  scanf("%s", user.password);

  FILE *fp = fopen("users.txt", "a+");
  if (fp == NULL)
  {
    printf("Failed to open file\n");
    exit(1);
  }

  char line[MAX_CHAR * 2];
  while (fgets(line, sizeof(line), fp) != NULL)
  {
    char savedUsername[MAX_CHAR];
    sscanf(line, "%[^;]", savedUsername);
    if (strcmp(user.username, savedUsername) == 0)
    {
      printf("Username already registered, Please Try Again.\n");
      fclose(fp);
      return;
    }
  }

  char hashedPassword[MAX_CHAR];
  strcpy(hashedPassword, GetMD5String(user.password, sizeof(user.password)));

  fprintf(fp, "%s;%s\n", user.username, hashedPassword);

  fclose(fp);

  printf("User registered successfully!\n");
}
```
Sementara itu, untuk proses login, nantinya username dan password yang diinput user akan dibandingkan pada file ``users.txt`` yang telah dibuat setelah user register. Jika sama, maka fungsi ``loginAccount()`` akan return value 1 ke fungsi ``main()``.

```c
int loginAccount()
{
  User user;
  printf("Enter your Username: ");
  scanf("%s", user.username);
  printf("Enter your Password: ");
  scanf("%s", user.password);

  FILE *fp = fopen("users.txt", "r");
  if (fp == NULL)
  {
    printf("Failed to open file\n");
    exit(1);
  }

  char line[MAX_CHAR * 2];
  char hashedPassword[MAX_CHAR];
  strcpy(hashedPassword, GetMD5String(user.password, sizeof(user.password)));

  while (fgets(line, sizeof(line), fp))
  {
    char savedUsername[MAX_CHAR];
    char savedPassword[MAX_CHAR];

    sscanf(line, "%[^;];%s", savedUsername, savedPassword);

    if (strcmp(user.username, savedUsername) == 0 && strcmp(hashedPassword, savedPassword) == 0)
    {
      fclose(fp);
      return 1;
    }
  }

  fclose(fp);
  return 0;
}
```
# Penjelasan Soal 5D
Untuk soal 5D, disini saya masih belum terlalu paham mengenai proses rename dengan memanggil fungsi fuse yang sudah ada. Jadinya untuk poin soal ini saya lewati.

# Penjelasan Soal 5E
Untuk soal 5E, kita diperintah untuk menampilkan seluruh directory yang ada didalam folder rahasia ketika sudah berhasil login. Oleh karena itu, setelah kembali dari fungsi ``loginAccount()`` akan dipanggil command system untuk memanggil directory rahasia yang ada di dalam docker container. Selain itu, kita juga akan menampilkan berapa banyak jumlah file dengan tipe file tertentu yang akan di echo ke file ``extensions.txt``. Berikut potongan kodenya.

```c
int logged = 0;
int login = loginAccount();
if (login == 1 && !logged)
{
logged = 1;
printf("Success to Login\n");

// tampilkan isi directory rahasia jika berhasil login
system("sudo docker exec -it container_rahasia /bin/bash -c 'ls /usr/share/rahasia'");
}

// poin E
int count_many(char *extension)
{
  for (int i = 0; i < typeIndex; i++)
  {
    if (strcmp(fileType[i], extension) == 0)
    {
      return i;
    }
  }
  return -1;
}

void count_fileType(const char *directory, int *directoryCount)
{
  DIR *dir;
  struct dirent *entry;
  char path[1000];

  dir = opendir(directory);

  if (dir == NULL)
  {
    printf("Failed to open directory: %s\n", directory);
    return;
  }

  while ((entry = readdir(dir)) != NULL)
  {
    if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
    {
      sprintf(path, "%s/%s", directory, entry->d_name);

      struct stat st;
      if (stat(path, &st) == 0)
      {
        if (S_ISDIR(st.st_mode))
        {
          (*directoryCount)++;
          count_fileType(path, directoryCount);
        }
        else if (S_ISREG(st.st_mode))
        {
          char *extension = strrchr(entry->d_name, '.');
          if (extension != NULL)
          {
            int extensionIndex = count_many(extension);
            if (extensionIndex != -1)
            {
              countType[extensionIndex]++;
            }
            else
            {
              strcpy(fileType[typeIndex], extension);
              countType[typeIndex]++;
              typeIndex++;
            }
          }
        }
      }
    }
  }

  closedir(dir);
}

int count = 0;
count_fileType("rahasia", &count);

char command[200];
system(command);
for (int i = 0; i < typeIndex; i++)
{
    command[0] = '\0';
    sprintf(command, "echo '%s: %d' >> extensions.txt", &fileType[i][1], countType[i]);
    system(command);
}
```
