#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <regex.h>

const char *dirpath = "/mnt/d/Code/Sistem_Operasi/Modul_4/soal4/shift4";

char* extract_module_path(const char* fullPath) {
    // Check if last character is a slash
    int fullPathLength = strlen(fullPath);
    char* fullPathCopy = strdup(fullPath);
    if (fullPathCopy[fullPathLength - 1] != '/') {
        // Add slash to the end of the string
        strcat(fullPathCopy, "/");
    }
    regex_t regex;
    const char* pattern = "^(.*/module_[^/]*)/";
    regmatch_t match[2];

    if (regcomp(&regex, pattern, REG_EXTENDED) != 0) {
        return NULL;
    }

    if (regexec(&regex, fullPathCopy, 2, match, 0) != 0) {
        return NULL;
    }

    int matchStart = match[1].rm_so;
    int matchEnd = match[1].rm_eo;
    int matchLength = matchEnd - matchStart;
    printf("Match Length: %d\n", matchLength);
    printf("Match Start: %d\n", matchStart);

    printf("Full path copy: %s\n", fullPathCopy);
    char* modulePath = malloc(matchLength + 1);
    strncpy(modulePath, fullPathCopy + matchStart, matchLength);
    modulePath[matchLength] = '\0';

    return modulePath;
}

int is_directory(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISDIR(path_stat.st_mode);
}

int is_file_parted(const char *filepath) {
    // Check if the file has multiple parts
    char *filename = strdup(filepath);
    int part_count = 1;
    while (1) {
        char part_filepath[1008];
        sprintf(part_filepath, "%s.%03d", filename, part_count);
        FILE *part_file = fopen(part_filepath, "r");
        if (part_file == NULL) {
            break;
        }
        fclose(part_file);
        part_count++;
    }
    free(filename);

    // If all parts exist, return true
    if (part_count > 1)
        return 1;
    return 0;
}

int modularization(const char *path)
{
    int chunk_size = 1024;
    FILE* file = fopen(path, "rb");
    if (file == NULL) {
        printf("Error can't open file.\n");
        return 1;
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    rewind(file);

    int numChunks = (file_size + chunk_size - 1) / chunk_size;

    char outputName[256];
    int chunkNumber = 1;

    while (!feof(file) && chunkNumber <= numChunks) {
        sprintf(outputName, "%s.%03d", path, chunkNumber);
        FILE* chunkFile = fopen(outputName, "wb");
        if (chunkFile == NULL) {
            printf("Error while creating chunk file.\n");
            break;
        }

        char* buffer = (char*)malloc(chunk_size);
        if (buffer == NULL) {
            printf("Error allocating memory.\n");
            break;
        }

        int bytesRead = fread(buffer, 1, chunk_size, file);
        fwrite(buffer, 1, bytesRead, chunkFile);

        free(buffer);
        fclose(chunkFile);
        chunkNumber++;
    }

    fclose(file);

    // Remove original file
    remove(path);

    return 0;
}

int combineChunks(const char* path)
{
    int chunkNumber = 1;
    char inputName[256];
    sprintf(inputName, "%s.%03d", path, chunkNumber);
    
    // Check if chunk file exists
    if (access(inputName, F_OK) == -1) {
        return 1;
    }

    char* outputFilename = strdup(path);
    FILE* outputFile = fopen(outputFilename, "wb");
    if (outputFile == NULL) {
        printf("Error creating output file.\n");
        return 1;
    }
    
    while (1) {
        FILE* chunkFile = fopen(inputName, "rb");
        if (chunkFile == NULL) {
            break;
        }
        printf("Combining chunk file %s\n", inputName);

        fseek(chunkFile, 0, SEEK_END);
        long chunkSize = ftell(chunkFile);
        rewind(chunkFile);

        char* buffer = (char*)malloc(chunkSize);
        if (buffer == NULL) {
            printf("Error allocating memory.\n");
            fclose(chunkFile);
            break;
        }

        int bytesRead = fread(buffer, 1, chunkSize, chunkFile);
        fwrite(buffer, 1, bytesRead, outputFile);

        free(buffer);
        fclose(chunkFile);
        printf("Successfully combined chunk file %s\n", inputName);

        printf("Removing chunk file %s\n", inputName);
        if (remove(inputName) == 0) {
            printf("Successfully removed chunk file %s\n", inputName);
        }
    
        chunkNumber++;
        sprintf(inputName, "%s.%03d", path, chunkNumber);
    }
    
    fclose(outputFile);

    return 0;
}

int read_combined_file_parts(const char *filepath, char *buf, size_t size, off_t offset) {
    printf("size: %ld\n", size);
    printf("offset: %ld\n", offset);
    // Read the combined content of the file parts
    char *filename = strdup(filepath);

    int part_count = 1;
    off_t current_offset = 0;
    size_t bytes_read = 0;

    while (1) {
        char part_filepath[1008];
        sprintf(part_filepath, "%s.%03d", filename, part_count);
        
        printf("Reading file part %s\n", part_filepath);
        
        FILE *part_file = fopen(part_filepath, "rb");
        if (part_file == NULL) {
            printf("Error can't open file %s\n", part_filepath);
            break;
        }

        fseek(part_file, 0, SEEK_END);
        long part_size = ftell(part_file);
        fseek(part_file, 0, SEEK_SET);

        if (current_offset < offset + size && current_offset + part_size > offset) {
            // Adjust the offset and size for the current part
            off_t part_offset = offset - current_offset;
            size_t part_size_adjusted = part_size - part_offset;
            if (part_size_adjusted > size - bytes_read)
                part_size_adjusted = size - bytes_read;

            // Read the part content into the buffer
            fseek(part_file, part_offset, SEEK_SET);
            size_t bytes_read_part = fread(buf + bytes_read, 1, part_size_adjusted, part_file);
            bytes_read += bytes_read_part;

            printf("Part size adjusted: %ld\n", part_size_adjusted);
            // If we have read the requested size or reached the end of the buffer, break
            printf("Bytes read part: %ld\n", bytes_read_part);
            if (bytes_read_part < (part_size_adjusted % part_count) || bytes_read >= size){
                // printf("Read %ld bytes from file part %s\n", bytes_read, part_filepath);
                break;
            }
        }

        fclose(part_file);
        part_count++;
        current_offset += part_size;
    }

    return bytes_read;
}

int do_modular(const char *path)
{
    DIR *dp;
    struct dirent *de;
    struct stat st;
    char filepath[260];

    dp = opendir(path);
    if (dp == NULL) {
        fprintf(stderr, "Error opening %s: %s\n", path, strerror(errno));
        return -1;
    }

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        sprintf(filepath, "%s/%s", path, de->d_name);

        if (de->d_type == DT_DIR) {
            do_modular(filepath);
        } else {
            stat(filepath, &st);
            
            // Check if size of file is greater than 1024
            if(st.st_size > 1024) {
                // Split file
                printf("File %s is being split...\n", filepath);
                modularization(filepath);
            }
            
        }
    }

    closedir(dp);
    return 0;
}

int do_demodular(const char* path)
{
    DIR *dp;
    struct dirent *de;
    struct stat st;
    char filepath[260];

    dp = opendir(path);
    if (dp == NULL) {
        fprintf(stderr, "Error opening %s: %s\n", path, strerror(errno));
        return -1;
    }

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        sprintf(filepath, "%s/%s", path, de->d_name);

        if (de->d_type == DT_DIR) {
            do_demodular(filepath);
        } else {
            stat(filepath, &st);
            
            regex_t regex;
            int reti;
            
            reti = regcomp(&regex, "\\.[0-9][0-9][0-9]$", 0);

            if (reti) {
                fprintf(stderr, "Could not compile regex\n");
                exit(1);
            }

            reti = regexec(&regex, filepath, 0, NULL, 0);
            if (!reti) {
                char *dot = strrchr(filepath, '.');
                if (dot != NULL) {
                    *dot = '\0';
                }
                // Combine file
                printf("File %s is being combined...\n", filepath);
                combineChunks(filepath);
            }
        }
    }

    closedir(dp);
    return 0;
}

int write_log(const char *level, const char *cmd_desc)
{
    FILE *log;
    log = fopen("/home/index/fs_module.log", "a");
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    char time[100];
    sprintf(time, "%02d%02d%04d-%02d:%02d:%02d", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
    fprintf(log, "%s::%s::%s\n", level, time, cmd_desc);
    fclose(log);
    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);

    // Check if path is modular folder
    char *modular_folder = extract_module_path(fpath);

    // Check if path is folder
    if (modular_folder != NULL && is_directory(modular_folder) && (strstr(modular_folder, "module_") != NULL)) {
        do_modular(modular_folder);
    }
    
    char desc[1024];
    sprintf(desc, "GETATTR::%s", path);
    
    write_log("REPORT", desc);
    
    res = lstat(fpath, stbuf);
    
    if (res == -1) {
        // Handle segmented file
        if(is_file_parted(fpath)) {
            // Iterate segmented file
            printf("File %s is segmented\n", fpath);
            int chunkNumber = 1;
            char chunkFilename[1500];
            
            sprintf(chunkFilename, "%s.%03d", fpath, chunkNumber);

            while(access(chunkFilename, F_OK) != -1) {
                struct stat st;
                stat(chunkFilename, &st);
                stbuf->st_mode = st.st_mode;
                stbuf->st_nlink = st.st_nlink;
                stbuf->st_uid = st.st_uid;
                stbuf->st_gid = st.st_gid;
                stbuf->st_size += st.st_size;
                stbuf->st_atime = st.st_atime;
                stbuf->st_mtime = st.st_mtime;
                stbuf->st_ctime = st.st_ctime;
                chunkNumber++;
                sprintf(chunkFilename, "%s.%03d", fpath, chunkNumber);
            }
            return 0;
        }
        return -errno;
    }

    return 0;
}

static int xmp_access(const char *path, int mask)
{
    int res;
    char fpath[1008];

    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "ACCESS::%s", path);

    write_log("REPORT", desc);

    res = access(fpath, mask);
    if (res == -1) {
        if (is_file_parted(fpath)) {
            return 0;
        }
        return -errno;
    }

    return 0;
}

static int xmp_readlink(const char *path, char *buf, size_t size)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "READLINK::%s", path);

    write_log("REPORT", desc);

    res = readlink(fpath, buf, size - 1);
    if (res == -1)
        return -errno;

    buf[res] = '\0';
    return 0;
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;

    (void) offset;
    (void) fi;

    char fpath[1008];
    if (strcmp(path, "/") == 0) {
        sprintf(fpath, "%s", dirpath);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    char desc[1024];
    sprintf(desc, "READDIR::%s", path);

    write_log("REPORT", desc);

    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        // Skip segmented files
        regex_t regex;
        int reti;
        reti = regcomp(&regex, "\\.[0-9][0-9][0-9]$", 0);

        if (reti) {
            fprintf(stderr, "Could not compile regex\n");
            exit(1);
        }

        reti = regexec(&regex, de->d_name, 0, NULL, 0);

        if (!reti) {
            if (strstr(de->d_name, ".001") != NULL) {
                de->d_name[strlen(de->d_name) - 4] = '\0';
            } else {
                continue;
            }
        }

        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (filler(buf, de->d_name, &st, 0))
            break;
    }

    closedir(dp);
    return 0;
}


static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
    int res;

    /* On Linux this could just be 'mknod(path, mode, rdev)' but this
       is more portable */
    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char desc[1024];
    sprintf(desc, "MKNOD::%s", path);

    write_log("REPORT", desc);

    if (S_ISREG(mode)) {
        res = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
        if (res >= 0)
            res = close(res);
    } else if (S_ISFIFO(mode))
        res = mkfifo(fpath, mode);
    else
        res = mknod(fpath, mode, rdev);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char desc[1024];
    sprintf(desc, "MKDIR::%s", path);

    write_log("REPORT", desc);
    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_unlink(const char *path)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char desc[1024];
    sprintf(desc, "UNLINK::%s", path);

    write_log("FLAG", desc);
    res = unlink(fpath);
    if (res == -1){
        if (is_file_parted(fpath)) {
            // delete all parts
            int chunkNumber = 1;
            char chunkPath[1024];
            sprintf(chunkPath, "%s.%03d", fpath, chunkNumber);
            while (access(chunkPath, F_OK) != -1) {
                unlink(chunkPath);
                chunkNumber++;
                sprintf(chunkPath, "%s.%03d", fpath, chunkNumber);
            }
        }
        return -errno;
    }

    return 0;
}

static int xmp_rmdir(const char *path)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char desc[1024];
    sprintf(desc, "RMDIR::%s", path);

    write_log("FLAG", desc);
    res = rmdir(fpath);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_symlink(const char *from, const char *to)
{
    int res;

    char ffrom[1000];
    if(strcmp(from,"/") == 0) {
        sprintf(ffrom,"%s",dirpath);
    } else sprintf(ffrom, "%s%s",dirpath,from);

    char fto[1000];
    if(strcmp(to,"/") == 0) {
        sprintf(fto,"%s",dirpath);
    } else sprintf(fto, "%s%s",dirpath,to);

    char desc[2048];
    sprintf(desc, "SYMLINK::%s::%s", from, to);

    write_log("REPORT", desc);

    res = symlink(ffrom, fto);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    int res;

    char ffrom[1000];
    if(strcmp(from,"/") == 0) {
        sprintf(ffrom,"%s",dirpath);
    } else sprintf(ffrom, "%s%s",dirpath,from);

    char fto[1000];
    if(strcmp(to,"/") == 0) {
        sprintf(fto,"%s",dirpath);
    } else sprintf(fto, "%s%s",dirpath,to);

    // Check if path is modular folder
    char *fto_modular_folder = extract_module_path(fto);
    char *ffrom_modular_folder = extract_module_path(ffrom);
    
    printf("FROM: %s\n", (ffrom_modular_folder == NULL) ? "NULL" : ffrom_modular_folder);
    printf("TO: %s\n", (fto_modular_folder == NULL) ? "NULL" : fto_modular_folder);
    
    if (fto_modular_folder == NULL) 
        fto_modular_folder = fto;
    if (ffrom_modular_folder == NULL)
        ffrom_modular_folder = ffrom;

    if (fto_modular_folder != NULL && (strstr(to, "module_") != NULL)) {
        if (strstr(from, "module_") == NULL) {
            printf("DOING MODULAR\n");
            do_modular(ffrom_modular_folder);
        }
    }

    if (ffrom_modular_folder != NULL && (strstr(from, "module_") != NULL)) {
        if (strstr(to, "module_") == NULL) {
            printf("DOING DEMODULAR\n");
            do_demodular(ffrom_modular_folder);
        }
    }

    // Check if path is file

    char desc[2048];
    sprintf(desc, "RENAME::%s::%s", from, to);

    write_log("REPORT", desc);

    res = rename(ffrom, fto);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_link(const char *from, const char *to)
{
    int res;

    char ffrom[1000];
    if(strcmp(from,"/") == 0) {
        sprintf(ffrom,"%s",dirpath);
    } else sprintf(ffrom, "%s%s",dirpath,from);

    char fto[1000];
    if(strcmp(to,"/") == 0) {
        sprintf(fto,"%s",dirpath);
    } else sprintf(fto, "%s%s",dirpath,to);

    char desc[2048];
    sprintf(desc, "LINK::%s::%s", from, to);

    write_log("REPORT", desc);

    res = link(ffrom, fto);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_chmod(const char *path, mode_t mode)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "CHMOD::%s", path);

    write_log("REPORT", desc);

    res = chmod(fpath, mode);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_chown(const char *path, uid_t uid, gid_t gid)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char desc[1024];
    sprintf(desc, "CHOWN::%s", path);

    write_log("REPORT", desc);

    res = lchown(fpath, uid, gid);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_truncate(const char *path, off_t size)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char desc[1024];
    sprintf(desc, "TRUNCATE::%s", path);

    write_log("REPORT", desc);

    res = truncate(fpath, size);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_utimens(const char *path, const struct timespec ts[2])
{
    int res;
    struct timeval tv[2];

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);

    tv[0].tv_sec = ts[0].tv_sec;
    tv[0].tv_usec = ts[0].tv_nsec / 1000;
    tv[1].tv_sec = ts[1].tv_sec;
    tv[1].tv_usec = ts[1].tv_nsec / 1000;
    
    char desc[1024];
    sprintf(desc, "UTIMENS::%s", path);

    write_log("REPORT", desc);

    res = utimes(fpath, tv);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char desc[1024];
    sprintf(desc, "OPEN::%s", path);

    write_log("REPORT", desc);
    res = open(fpath, fi->flags);
    if (res == -1) {
        if(is_file_parted(fpath)) {
            char fpath2[1012];
            sprintf(fpath2, "%s.001", fpath);
            printf("OPEN %s\n", fpath2);
            res = open(fpath2, fi->flags);
            return 0;
        } else {
            return -errno;
        }
    }

    close(res);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;

    (void) fi;
    char fpath[1008];
    if (strcmp(path, "/") == 0) {
        sprintf(fpath, "%s", dirpath);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    char desc[1024];
    sprintf(desc, "READ::%s", path);

    write_log("REPORT", desc);

    // Check if the file has multiple parts
    if (is_file_parted(fpath)) {
        res = read_combined_file_parts(fpath, buf, size, offset);
        if (res == -1)
            return -errno;
        return res;
    }

    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static int xmp_write(const char *path, const char *buf, size_t size,
                     off_t offset, struct fuse_file_info *fi)
{

// actually we don't need to look at the 'open' calls - when
// we call 'write', flag the file's private data with the
// fact that the file was updated, so that we can take a
// copy when we get the 'release' call.

    int fd;
    int res;

    (void) fi;
    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "WRITE::%s", path);

    write_log("REPORT", desc);

    fd = open(fpath, O_WRONLY);
    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static int xmp_statfs(const char *path, struct statvfs *stbuf)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "STATFS::%s", path);

    write_log("REPORT", desc);

    res = statvfs(fpath, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_release(const char *path, struct fuse_file_info *fi)
{
// Release is called when there are no more open handles.  This is where
// we do whatever action we want to with the file as all updates are
// now complete.  For example, calling gpg to encrypt it, or rsync
// to transfer it to disaster-recovery storage

// OR look at fi->flags for write access, and assume if opened
// for write, it will have been written to

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    // Check if path is modular folder
    char *modular_folder = extract_module_path(fpath);
    printf("modular_folder: %s\n", modular_folder);
    
    // Check if path is folder
    if (modular_folder != NULL && is_directory(modular_folder) && (strstr(modular_folder, "module_") != NULL)) {
        do_modular(modular_folder);
    }

    char desc[1024];
    sprintf(desc, "RELEASE::%s", path);

    write_log("REPORT", desc);

    (void) path;
    (void) fi;
    return 0;
}

static int xmp_fsync(const char *path, int isdatasync,
                     struct fuse_file_info *fi)
{
    /* Just a stub.  This method is optional and can safely be left
       unimplemented */

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "FSYNC::%s", path);

    write_log("REPORT", desc);
    (void) path;
    (void) isdatasync;
    (void) fi;
    return 0;
}

#ifdef HAVE_SETXATTR
/* xattr operations are optional and can safely be left unimplemented */
static int xmp_setxattr(const char *path, const char *name, const char *value,
                        size_t size, int flags)
{
    int res;
    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "SETXATTR::%s", path);

    write_log("REPORT", desc);

    res = lsetxattr(fpath, name, value, size, flags);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_getxattr(const char *path, const char *name, char *value,
                    size_t size)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "GETXATTR::%s", path);

    write_log("REPORT", desc);

    res = lgetxattr(fpath, name, value, size);
    if (res == -1)
        return -errno;
    return res;
}

static int xmp_listxattr(const char *path, char *list, size_t size)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "LISTXATTR::%s", path);

    write_log("REPORT", desc);

    res = llistxattr(fpath, list, size);
    if (res == -1)
        return -errno;
    return res;
}

static int xmp_removexattr(const char *path, const char *name)
{
    int res;

    char fpath[1008];
    if(strcmp(path,"/") == 0) {
        sprintf(fpath,"%s",dirpath);
    } else sprintf(fpath, "%s%s",dirpath,path);
    
    char desc[1024];
    sprintf(desc, "REMOVEXATTR::%s", path);

    write_log("REPORT", desc);

    res = lremovexattr(fpath, name);
    if (res == -1)
        return -errno;
    return 0;
}
#endif /* HAVE_SETXATTR */

static struct fuse_operations xmp_oper = {
    .getattr	= xmp_getattr,
    .access	= xmp_access,
    .readlink	= xmp_readlink,
    .readdir	= xmp_readdir,
    .mknod	= xmp_mknod,
    .mkdir	= xmp_mkdir,
    .symlink	= xmp_symlink,
    .unlink	= xmp_unlink,
    .rmdir	= xmp_rmdir,
    .rename	= xmp_rename,
    .link	= xmp_link,
    .chmod	= xmp_chmod,
    .chown	= xmp_chown,
    .truncate	= xmp_truncate,
    .utimens	= xmp_utimens,
    .open	= xmp_open,
    .read	= xmp_read,
    .write	= xmp_write,
    .statfs	= xmp_statfs,
    .release	= xmp_release,
    .fsync	= xmp_fsync,
#ifdef HAVE_SETXATTR
    .setxattr	= xmp_setxattr,
    .getxattr	= xmp_getxattr,
    .listxattr	= xmp_listxattr,
    .removexattr= xmp_removexattr,
#endif
};

int main(int argc, char *argv[])
{
    int rc;

    umask(0);
    rc = fuse_main(argc, argv, &xmp_oper, NULL);

    return rc;
}
