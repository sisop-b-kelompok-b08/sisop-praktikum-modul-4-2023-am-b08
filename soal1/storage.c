#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

#define MAX_PLAYERS 1000
#define MAX_LINE_LENGTH 2048

typedef struct {
    int id;
    char name[100];
    int age;
    char photo[200];
    char nationality[100];
    char flag[200];
    int overall;
    int potential;
    char club[100];
    char clubLogo[200];
    char value[20];
    char wage[20];
    int special;
    char preferredFoot[10];
    float internationalReputation;
    float weakFoot;
    float skillMoves;
    char workRate[50];
    char bodyType[50];
    char realFace[10];
    char position[50];
    char joined[50];
    char loanedFrom[100];
    char contractValidUntil[50];
    char height[10];
    char weight[10];
    char releaseClause[20];
    int kitNumber;
    int bestOverallRating;
} Player;

int extractZip(const char* zipFilePath, const char* destFolderPath) {
    char command[256];
    sprintf(command, "unzip %s -d %s", zipFilePath, destFolderPath);

    if (system(command) == -1) {
        printf("Failed to extract zip file.\n");
        return -1;
    }
    return 0;
}

int readPlayersCSV(const char* csvFilePath, Player* players, int maxPlayers) {
    FILE* file = fopen(csvFilePath, "r");
    if (file == NULL) {
        printf("Failed to open CSV file: %s\n", csvFilePath);
        return -1;
    }
    char line[MAX_LINE_LENGTH];
    int count = 0;

    fgets(line, MAX_LINE_LENGTH, file); // Skip header line

    while (fgets(line, MAX_LINE_LENGTH, file) != NULL) {
        if (count >= maxPlayers) {
            printf("Maximum number of players reached.\n");
            break;
        }

        Player player;
        sscanf(line, "%d,%99[^,],%d,%199[^,],%99[^,],%199[^,],%d,%d,%99[^,],%199[^,],%19[^,],%19[^,],%d,%9[^,],%f,%f,%f,%199[^,],%49[^,],%9[^,],%49[^,],%199[^,],%99[^,],%49[^,],%9[^,],%9[^,],%19[^,],%d,%d",
       &player.id, player.name, &player.age, player.photo, player.nationality, player.flag,
       &player.overall, &player.potential, player.club, player.clubLogo, player.value, player.wage,
       &player.special, player.preferredFoot, &player.internationalReputation, &player.weakFoot,
       &player.skillMoves, player.workRate, player.bodyType, player.realFace, player.position,
       player.joined, player.loanedFrom, player.contractValidUntil, player.height, player.weight,
       player.releaseClause, &player.kitNumber, &player.bestOverallRating);


        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0) {
            printf("Name: %s\n", player.name);
            printf("Age: %d\n", player.age);
            printf("Nationality: %s\n", player.nationality);
            printf("Club: %s\n", player.club);
            printf("Potential: %d\n", player.potential);
            printf("Photo URL: %s\n", player.photo);
            // Print other desired information

            printf("\n");
        }
        players[count++] = player;
    }
    fclose(file);
    return count;
}

int main() {
    const char* zipFilePath = "fifa-player-stats-database.zip";
    const char* destFolderPath = "."; // Extract to current directory
    const char* csvFilePath = "FIFA23_official_data.csv";
    Player players[MAX_PLAYERS];

    if (extractZip(zipFilePath, destFolderPath) != 0) {
        printf("Extraction failed.\n");
        return -1;
    }

    int playerCount = readPlayersCSV(csvFilePath, players, MAX_PLAYERS);
    if (playerCount < 0) {
        printf("Failed to read players CSV.\n");
        return -1;
    }
    printf("Analysis complete.\n");
    return 0;
}