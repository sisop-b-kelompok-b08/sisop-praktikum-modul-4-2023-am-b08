#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <openssl/md5.h>

#define MAX_CHAR 100

char fileType[100][50]; 
int typeIndex = 0;
int countType[100] = {0};

static const char *dirpath = "/home/ahda/sisop/modul4/rahasia";

static int
xmp_getattr(const char *path, struct stat *stbuf)
{
  int res;
  char fpath[1000];

  sprintf(fpath, "%s%s", dirpath, path);

  res = lstat(fpath, stbuf);

  if (res == -1)
    return -errno;

  return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  char fpath[1000];

  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;

  dp = opendir(fpath);

  if (dp == NULL)
    return -errno;

  while ((de = readdir(dp)) != NULL)
  {

    struct stat st;

    memset(&st, 0, sizeof(st));

    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (strstr(de->d_name, "B08") == NULL)
    {
      if (S_ISDIR(st.st_mode) && strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0)
      {
        char oldPath[1500];
        char newPath[1500];
        sprintf(oldPath, "%s/%s", fpath, de->d_name);
        sprintf(newPath, "%s/%s_B08", fpath, de->d_name);
        rename(oldPath, newPath);
        printf("%s\n", newPath);
      }
      else if (S_ISREG(st.st_mode))
      {
        char oldPath[1500];
        char newPath[1500];
        sprintf(oldPath, "%s/%s", fpath, de->d_name);
        sprintf(newPath, "%s/B08_%s", fpath, de->d_name);
        rename(oldPath, newPath);
        printf("%s\n", newPath);
      }
    }

    res = (filler(buf, de->d_name, &st, 0));

    if (res != 0)
      break;
  }

  closedir(dp);

  return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;

    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;
  int fd = 0;

  (void)fi;

  fd = open(fpath, O_RDONLY);

  if (fd == -1)
    return -errno;

  res = pread(fd, buf, size, offset);

  if (res == -1)
    res = -errno;

  close(fd);

  return res;
}

static int xmp_opendir(const char *path, struct fuse_file_info *fi)
{

  int res = 0;
  char fpath[1000];

  sprintf(fpath, "%s%s", dirpath, path);

  DIR *dp = opendir(fpath);

  if (dp == NULL)
    return -errno;

  fi->fh = (intptr_t)dp;

  return res;
}

static int xmp_releasedir(const char *path, struct fuse_file_info *fi)
{
  int res = 0;

  (void)path;
  closedir((DIR *)(intptr_t)fi->fh);

  return res;
}

static int xmp_rename(const char *from, const char *to)
{
  char oldPath[1000], newPath[1000];
  sprintf(oldPath, "%s%s", dirpath, from);
  sprintf(newPath, "%s%s", dirpath, to);

  int result = rename(oldPath, newPath);
  if (result == -1)
    return -errno;

  return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .opendir = xmp_opendir,
    .releasedir = xmp_releasedir,
    .rename = xmp_rename,
};

typedef struct
{
  char username[MAX_CHAR];
  char password[MAX_CHAR];
} User;

typedef union uwb {
	unsigned w;
	unsigned char b[4];
} MD5union;

typedef unsigned DigestArray[4];

unsigned func0(unsigned abcd[]) {
	return (abcd[1] & abcd[2]) | (~abcd[1] & abcd[3]);
}

unsigned func1(unsigned abcd[]) {
	return (abcd[3] & abcd[1]) | (~abcd[3] & abcd[2]);
}

unsigned func2(unsigned abcd[]) {
	return  abcd[1] ^ abcd[2] ^ abcd[3];
}

unsigned func3(unsigned abcd[]) {
	return abcd[2] ^ (abcd[1] | ~abcd[3]);
}

typedef unsigned(*DgstFctn)(unsigned a[]);

unsigned *calctable(unsigned *k)
{
	double s, pwr;
	int i;

	pwr = pow(2.0, 32);
	for (i = 0; i<64; i++) {
		s = fabs(sin(1.0 + i));
		k[i] = (unsigned)(s * pwr);
	}
	return k;
}

unsigned rol(unsigned r, short N)
{
	unsigned  mask1 = (1 << N) - 1;
	return ((r >> (32 - N)) & mask1) | ((r << N) & ~mask1);
}

unsigned* Algorithms_Hash_MD5(const char *msg, int mlen)
{
	static DigestArray h0 = { 0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476 };
	static DgstFctn ff[] = { &func0, &func1, &func2, &func3 };
	static short M[] = { 1, 5, 3, 7 };
	static short O[] = { 0, 1, 5, 0 };
	static short rot0[] = { 7, 12, 17, 22 };
	static short rot1[] = { 5, 9, 14, 20 };
	static short rot2[] = { 4, 11, 16, 23 };
	static short rot3[] = { 6, 10, 15, 21 };
	static short *rots[] = { rot0, rot1, rot2, rot3 };
	static unsigned kspace[64];
	static unsigned *k;

	static DigestArray h;
	DigestArray abcd;
	DgstFctn fctn;
	short m, o, g;
	unsigned f;
	short *rotn;
	union {
		unsigned w[16];
		char     b[64];
	}mm;
	int os = 0;
	int grp, grps, q, p;
	unsigned char *msg2;

	if (k == NULL) k = calctable(kspace);

	for (q = 0; q<4; q++) h[q] = h0[q];

	{
		grps = 1 + (mlen + 8) / 64;
		msg2 = (unsigned char*)malloc(64 * grps);
		memcpy(msg2, msg, mlen);
		msg2[mlen] = (unsigned char)0x80;
		q = mlen + 1;
		while (q < 64 * grps) { msg2[q] = 0; q++; }
		{
			MD5union u;
			u.w = 8 * mlen;
			q -= 8;
			memcpy(msg2 + q, &u.w, 4);
		}
	}

	for (grp = 0; grp<grps; grp++)
	{
		memcpy(mm.b, msg2 + os, 64);
		for (q = 0; q<4; q++) abcd[q] = h[q];
		for (p = 0; p<4; p++) {
			fctn = ff[p];
			rotn = rots[p];
			m = M[p]; o = O[p];
			for (q = 0; q<16; q++) {
				g = (m*q + o) % 16;
				f = abcd[1] + rol(abcd[0] + fctn(abcd) + k[q + 16 * p] + mm.w[g], rotn[q % 4]);

				abcd[0] = abcd[3];
				abcd[3] = abcd[2];
				abcd[2] = abcd[1];
				abcd[1] = f;
			}
		}
		for (p = 0; p<4; p++)
			h[p] += abcd[p];
		os += 64;
	}
	return h;
}

const char* GetMD5String(const char *msg, int mlen) {
	char str[33];
	strcpy(str, "");
	int j;
	unsigned *d = Algorithms_Hash_MD5(msg, strlen(msg));
	MD5union u;
	for (j = 0; j<4; j++) {
		u.w = d[j];
		char s[9];
		sprintf(s, "%02x%02x%02x%02x", u.b[0], u.b[1], u.b[2], u.b[3]);
		strcat(str, s);
	}

	return strdup(str);
}

void registerAccount()
{
  User user;
  printf("Enter your Username: ");
  scanf("%s", user.username);
  printf("Enter your Password: ");
  scanf("%s", user.password);

  FILE *fp = fopen("users.txt", "a+");
  if (fp == NULL)
  {
    printf("Failed to open file\n");
    exit(1);
  }

  char line[MAX_CHAR * 2];
  while (fgets(line, sizeof(line), fp) != NULL)
  {
    char savedUsername[MAX_CHAR];
    sscanf(line, "%[^;]", savedUsername);
    if (strcmp(user.username, savedUsername) == 0)
    {
      printf("Username already registered, Please Try Again.\n");
      fclose(fp);
      return;
    }
  }

  char hashedPassword[MAX_CHAR];
  strcpy(hashedPassword, GetMD5String(user.password, sizeof(user.password)));

  fprintf(fp, "%s;%s\n", user.username, hashedPassword);

  fclose(fp);

  printf("User registered successfully!\n");
}

int loginAccount()
{
  User user;
  printf("Enter your Username: ");
  scanf("%s", user.username);
  printf("Enter your Password: ");
  scanf("%s", user.password);

  FILE *fp = fopen("users.txt", "r");
  if (fp == NULL)
  {
    printf("Failed to open file\n");
    exit(1);
  }

  char line[MAX_CHAR * 2];
  char hashedPassword[MAX_CHAR];
  strcpy(hashedPassword, GetMD5String(user.password, sizeof(user.password)));

  while (fgets(line, sizeof(line), fp))
  {
    char savedUsername[MAX_CHAR];
    char savedPassword[MAX_CHAR];

    sscanf(line, "%[^;];%s", savedUsername, savedPassword);

    if (strcmp(user.username, savedUsername) == 0 && strcmp(hashedPassword, savedPassword) == 0)
    {
      fclose(fp);
      return 1;
    }
  }

  fclose(fp);
  return 0;
}

int count_many(char *extension)
{
  for (int i = 0; i < typeIndex; i++)
  {
    if (strcmp(fileType[i], extension) == 0)
    {
      return i;
    }
  }
  return -1;
}

void count_fileType(const char *directory, int *directoryCount)
{
  DIR *dir;
  struct dirent *entry;
  char path[1000];

  dir = opendir(directory);

  if (dir == NULL)
  {
    printf("Failed to open directory: %s\n", directory);
    return;
  }

  while ((entry = readdir(dir)) != NULL)
  {
    if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
    {
      sprintf(path, "%s/%s", directory, entry->d_name);

      struct stat st;
      if (stat(path, &st) == 0)
      {
        if (S_ISDIR(st.st_mode))
        {
          (*directoryCount)++;
          count_fileType(path, directoryCount);
        }
        else if (S_ISREG(st.st_mode))
        {
          char *extension = strrchr(entry->d_name, '.');
          if (extension != NULL)
          {
            int extensionIndex = count_many(extension);
            if (extensionIndex != -1)
            {
              countType[extensionIndex]++;
            }
            else
            {
              strcpy(fileType[typeIndex], extension);
              countType[typeIndex]++;
              typeIndex++;
            }
          }
        }
      }
    }
  }

  closedir(dir);
}

// Download dan Unzip
void downloadZip(){
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }
    if (child_id == 0){
        char *url = "https://drive.google.com/u/0/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=t&uuid=69f3b1f9-0761-4b5b-a766-346b671757fe&at=AKKF8vyHKxQz_HFZAZ1l_4H675Rb:1684825030868";
        char *argv[] = {"wget", "--no-check-certificate", "-q", "-O", "rahasia.zip", url, NULL};
        execv("/bin/wget", argv);
    } else {
        while ((wait(&status)) > 0);
        return;
    }  
}

void unzip(){
    pid_t child_id;
    child_id = fork();
    int status;

    if (child_id < 0){
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        char *argv[] = {"unzip", "-qq", "rahasia.zip", "-d", "/home/ahda/sisop/modul4/rahasia", NULL};
        execv("/bin/unzip", argv);
    } else {
        while ((wait(&status)) > 0);
        return;
    }
}

int main(int argc, char *argv[])
{
  pid_t pid = fork();

  if (pid == 0){
    // poin A
    //downloadZip();
    //unzip();
  }
  else {
    wait(NULL);

    // poin E
    int count = 0;
    count_fileType("rahasia", &count);

    char command[200];
    system(command);
    for (int i = 0; i < typeIndex; i++)
    {
      command[0] = '\0';
      sprintf(command, "echo '%s: %d' >> extensions.txt", &fileType[i][1], countType[i]);
      system(command);
    }
    
    pid_t pid2 = fork();
    if (pid2 == 0){
      umask(0);
      fuse_main(argc, argv, &xmp_oper, NULL);
    }
    else {
      // Poin B : Mount Folder ke Docker Container dengan Docker Compose
      system("sudo docker pull ubuntu:focal");
      system("sudo docker-compose up -d");

      // Poin C : Sistem Registrasi dan Login
      while(1){
    
        int status;
        printf("Welcome to Rahasia Encrypted Company, Choose Option:\n");
        printf("1. Register\n");
        printf("2. Login\n");
        printf("3. Exit\n");
        scanf("%d", &status);

        if (status == 1)
        {
          registerAccount();
        }
        else if (status == 2)
        {
          int logged = 0;
          int login = loginAccount();
          if (login == 1 && !logged)
          {
            logged = 1;
            printf("Success to Login\n");

            // tampilkan isi directory rahasia jika berhasil login
            system("sudo docker exec -it container_rahasia /bin/bash -c 'ls /usr/share/rahasia'");
          }
          else
          {
            printf("Failed to Login, Username and Passwords Does Not Match.\n");
          }
        }
        else break;
      }
      

    }
  }

}
